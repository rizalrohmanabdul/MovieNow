# Movie Now

This is a movie application built using Kotlin and follows the MVVM architecture pattern. The application uses the Paging 3 library for pagination, Dagger Hilt for dependency injection, Coroutines for asynchronous programming.


## Architecture

The application follows the Model-View-ViewModel (MVVM) architecture pattern. The components are organized as follows:

- Model: Holds the data and the business logic. In this app, the repository is responsible for retrieving data from the remote data source.
- View: Displays the UI and receives user input. In this app, the views are implemented using fragments.
- ViewModel: Acts as a mediator between the model and the view. In this app, the view models are responsible for exposing data to the views and handling user interactions.


## Dependencies

The application uses the following dependencies:

- [Kotlin]("https://kotlinlang.org/") as the programming language.
- [Paging 3]("https://developer.android.com/topic/libraries/architecture/paging/v3-overview") for pagination.
- [Dagger Hilt]("https://dagger.dev/hilt/") for dependency injection.
- [Coroutines]("https://kotlinlang.org/docs/coroutines-overview.html") for asynchronous programming.


## Installation

Clone the repository using the following command:

```bash
  git clone https://gitlab.com/rizalrohmanabdul/MovieNow.git
```
Open the project in Android Studio and run the app on an emulator or a physical device.

## License

This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/)


